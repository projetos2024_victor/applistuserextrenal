import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";

const List = () => {
    const navigation = useNavigation(); // Use useNavigation hook here

    const [users, setUsers] = useState([]);

    useEffect(() => {
        getUsers();
    }, []);

    async function getUsers() {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            setUsers(response.data);
        } catch (error) {
            console.error("Error fetching users:", error);
        }
    }

    const taskPress = (user) => {
        navigation.navigate('Details', { user }); // Pass 'user', not 'users'
    };

    return (
        <View>
            <Text> Lista de Usuários </Text>
            <FlatList
                data={users}
                keyExtractor={(item) => item.id.toString()} // Add parentheses to item.id.toString
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => taskPress(item)}>
                        <Text>{item.name}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
};

export default List;

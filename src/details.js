import React from "react";
import { View, Text } from "react-native";

const Details = ({ route }) => {
    const { user } = route.params;

    return (
        <View>
            <Text>Details:</Text>
            <Text>Nome: {user.username}</Text>
            <Text>Email: {user.email}</Text>
            <Text>Rua: {user.address.street}</Text>
            <Text>Cidade: {user.address.city}</Text>
            <Text>Conjunto de Endereços: {user.address.suite}</Text>
            <Text>Telefone: {user.phone}</Text>
            <Text>Local na rede Internet: {user.website}</Text>
            <Text>Nome da Empresa: {user.company.name}</Text>
            <Text>Frase de efeito da empresa: {user.company.catchPhrase}</Text>

        </View>
    );
};

export default Details;
